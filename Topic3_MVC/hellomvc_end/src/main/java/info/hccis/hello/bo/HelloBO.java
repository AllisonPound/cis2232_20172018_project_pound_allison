package info.hccis.hello.bo;

import info.hccis.hello.entity.Person;

/**
 * This will contain business logic associated with our hello world application.
 * @author bjmaclean
 * @since Oct 6, 2017
 */
public class HelloBO {
    public void sayHelloToConsole(String name){
        System.out.println("Hello "+name);
    }
    public void savePerson(Person person){
        System.out.println("would be saving this info to the database");
        System.out.println(person);
    }
}
