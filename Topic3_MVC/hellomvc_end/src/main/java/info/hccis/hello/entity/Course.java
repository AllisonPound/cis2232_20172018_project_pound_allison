package info.hccis.hello.entity;

import java.util.ArrayList;

/**
 * 
 * @since Oct 11, 2017
 * @author bjmaclean
 */
public class Course {

    private ArrayList<String> courses = new ArrayList();

    public Course() {
        courses.add("CIS1201");
        courses.add("CIS1280");
        courses.add("CIS1300");
        courses.add("CIS1360");
        courses.add("CIS1150");
    }

    public ArrayList<String> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<String> courses) {
        this.courses = courses;
    }
    
    
}
