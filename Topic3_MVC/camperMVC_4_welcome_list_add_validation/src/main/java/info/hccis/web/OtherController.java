package info.hccis.web;

import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.entity.Camper;
import java.util.ArrayList;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    @RequestMapping("/")
    public String showHome(Model model) {

        
        //This will send the user to the welcome.html page.
        return "welcome";
    }

    @RequestMapping("/about")
    public String showAbout(Model model) {
        return "other/about";
    }

        @RequestMapping("/help")
    public String showHelp(Model model) {
        return "other/help";
    }

}
