package info.hccis.camper.web;

import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.entity.Camper;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CamperController {

    @RequestMapping("/camper/list")
    public String showHome(Model model) {

        //Get the campers from the database
        ArrayList<Camper> campers = CamperDAO.selectAll();
        System.out.println("BJM-found " + campers.size() + " campers.  Going to welcome page");
        model.addAttribute("campers", campers);

        //This will send the user to the welcome.html page.
        return "camper/list";
    }

    @RequestMapping("/camper/add")
    public String addCamper(Model model) {

        model.addAttribute("camper", new Camper());

        //This will send the user to the welcome.html page.
        return "camper/add";
    }

    @RequestMapping("/camper/addSubmit")
    public String addSubmitCamper(Model model, @ModelAttribute("camper") Camper camper) {

        System.out.println("BJM-about to save a camper" + camper);
        try {
            CamperDAO.update(camper);
            //Get the campers from the database
            ArrayList<Camper> campers = CamperDAO.selectAll();
            model.addAttribute("campers", campers);
        } catch (Exception ex) {
            System.out.println("Error adding camper");
        }

        //This will send the user to the welcome.html page.
        return "camper/list";
    }

}
