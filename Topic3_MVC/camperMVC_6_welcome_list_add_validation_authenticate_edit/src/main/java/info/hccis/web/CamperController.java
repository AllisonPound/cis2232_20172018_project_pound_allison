package info.hccis.web;

import info.hccis.camper.bo.CamperValidationBO;
import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.entity.Camper;
import info.hccis.camper.entity.User;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CamperController {

    @RequestMapping("/camper/add")
    public String add(Model model, HttpSession session) {

                //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if(user == null){
            model.addAttribute("user", new User());
            return "other/welcome";
        }

        
        Camper camper = new Camper();
        model.addAttribute("camper", camper);

        return "camper/add";
    }

    @RequestMapping("/camper/edit")
    public String edit(Model model, HttpSession session, HttpServletRequest request) {

                //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if(user == null){
            model.addAttribute("user", new User());
            return "other/welcome";
        }

        String id = request.getParameter("id");
        Camper camper = CamperDAO.select(Integer.parseInt(id));
        model.addAttribute("camper", camper);

        return "camper/add";
    }

    
    
    
    @RequestMapping("/camper/addSubmit")
    public String addSubmit(Model model, @ModelAttribute("camper") Camper camper, HttpSession session) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if(user == null){
            model.addAttribute("user", new User());
            return "other/welcome";
        }

        
        ArrayList<String> errors = CamperValidationBO.validateCamper(camper);
        //If there is an error send them back to add page.
        
        boolean error = false;
        if(!errors.isEmpty()){
            error = true;
        }
        
        if (error) {
            model.addAttribute("messages", errors);
            return "/camper/add";
        }

        System.out.println("BJM - About to add " + camper + " to the database");
        try {
            CamperDAO.update(camper);
            //Get the campers from the database
            ArrayList<Camper> campers = CamperDAO.selectAll();
            model.addAttribute("campers", campers);
        } catch (Exception ex) {
            System.out.println("BJM - There was an error adding camper to the database");
        }
        return "camper/list";
    }

    @RequestMapping("/camper/list")
    public String showHome(Model model, HttpSession session) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if(user == null){
            model.addAttribute("user", new User());
            return "other/welcome";
        }
        
        //Get the campers from the database
        ArrayList<Camper> campers = CamperDAO.selectAll();
        System.out.println("BJM-found " + campers.size() + " campers.  Going to welcome page");
        model.addAttribute("campers", campers);

        //This will send the user to the welcome.html page.
        return "camper/list";
    }

}
