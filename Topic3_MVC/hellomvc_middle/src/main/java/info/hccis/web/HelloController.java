package info.hccis.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {

    @RequestMapping("/")
    public String showHome(Model model) {
        
        //This will send the user to the welcome.html page.
        return "hello/welcome";
    }

    @RequestMapping("/sayhello")
    public String sayHello(Model model) {
        
        System.out.println("BJM - Hello");
        
        //This will send the user to the welcome.html page.
        return "hello/newOne";
    }


    @RequestMapping("/newOne")
    public String showNewOne(Model model) {
        System.out.println("in controller for /newOne");
        return "camper/newOne";

        /*
        After this have to make sure that this html page exists. This would be 
        in the WEB-INF/thymeleaf/camper/
         */
    }

}
