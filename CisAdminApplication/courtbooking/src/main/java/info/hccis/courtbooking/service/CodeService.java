package info.hccis.courtbooking.service;

import info.hccis.courtbooking.data.springdatajpa.CodeTypeRepository;
import info.hccis.courtbooking.data.springdatajpa.UserRepository;
import info.hccis.courtbooking.model.DatabaseConnection;
import info.hccis.courtbooking.model.jpa.User;
import info.hccis.courtbooking.model.jpa.CodeType;
import java.util.ArrayList;


/**
 * Code service 
 *
 * @author BJ MacLean
 */
public interface CodeService {

    public abstract CodeTypeRepository getCtr();
    public abstract UserRepository getUr();
    
    /**
     *
     * @param codeTypeId
     * @return
     */
    public abstract ArrayList<CodeType> getCodeTypes();
    
    /**
     *
     * @param codeTypeId
     * @return
     */
    public abstract ArrayList<CodeType> getCodeTypes(DatabaseConnection databaseConnection);

    /**
     *
     * @param codeTypeId
     * @return
     */
    public abstract ArrayList<User> getUsers();

    
}
