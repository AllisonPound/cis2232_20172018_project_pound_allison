package info.hccis.courtbooking.data.springdatajpa;

import info.hccis.courtbooking.model.jpa.CodeValue;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodeValueRepository extends CrudRepository<CodeValue, Integer> {
    List<CodeValue> findByCodeTypeId(int codeTypeId);
    CodeValue findByCodeValueSequence(int codeValueSequence);
      List<CodeValue> findByEnglishDescription(String englishDescription);
}