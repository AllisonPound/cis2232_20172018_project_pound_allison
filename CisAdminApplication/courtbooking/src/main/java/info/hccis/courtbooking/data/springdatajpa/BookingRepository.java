package info.hccis.courtbooking.data.springdatajpa;

import info.hccis.courtbooking.model.jpa.Booking;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingRepository extends CrudRepository<Booking, Integer> {
   List<Booking> findByCourtId(int courtId);
   List<Booking> findByBookingDate(String bookingDate);
   List<Booking> findByUserId(int userId);
   Booking findByBookingId(int bookingId);
   
   
    


}