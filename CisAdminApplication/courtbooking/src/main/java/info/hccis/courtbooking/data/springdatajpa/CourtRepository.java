package info.hccis.courtbooking.data.springdatajpa;

import info.hccis.courtbooking.model.jpa.Court;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourtRepository extends CrudRepository<Court, Integer> {
   List<Court> findByCourtType(int courtType);

    public Court findByCourtId(int courtId);
    


}