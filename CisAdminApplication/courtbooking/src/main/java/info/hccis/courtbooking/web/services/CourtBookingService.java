/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.courtbooking.web.services;

import com.google.gson.Gson;
import info.hccis.courtbooking.dao.BookingDAO;
import info.hccis.courtbooking.model.jpa.Court;
import java.util.ArrayList;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author AllisonPound
 * @since 20171213
 */
@Path("/CourtBookingService")
public class CourtBookingService {

    @GET
    @Path("/availableCourts/{startDate}/{endDate}/{courtType}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAvailableCourts(@PathParam("startDate") String startDate, @PathParam("endDate") String endDate, @PathParam("courtType") int courtType) {
 // public Response getAvailableCourts() {                
        //Get the availableCourts from the database
        ArrayList<Court> courts = BookingDAO.getAvailableBookings(startDate, endDate, courtType);
        Gson gson = new Gson();
        String temp = "";
        temp = gson.toJson(courts);

        int statusCode = 200;
        if (courts.isEmpty()) {
            statusCode = 204;
        }
        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }

}
