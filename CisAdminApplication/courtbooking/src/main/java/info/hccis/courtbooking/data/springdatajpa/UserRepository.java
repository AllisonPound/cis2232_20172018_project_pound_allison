package info.hccis.courtbooking.data.springdatajpa;

import info.hccis.courtbooking.model.jpa.User;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
       List<User> findByUsername(String username);
       User findByUserId(int userId);


}