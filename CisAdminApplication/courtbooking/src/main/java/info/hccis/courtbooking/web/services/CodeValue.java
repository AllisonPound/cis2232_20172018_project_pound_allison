package info.hccis.courtbooking.web.services;

import info.hccis.courtbooking.dao.CodeValueDAO;
import info.hccis.courtbooking.model.DatabaseConnection;
import java.util.ArrayList;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author BJ
 */
@WebService(serviceName = "CodeValue")
public class CodeValue {

    /**
     * This is a sample web service operation
     * @return 
     */
    
    //to use this in the webapp
    //http://stackoverflow.com/questions/23011547/webservice-client-generation-error-with-jdk8
    @WebMethod(operationName = "list")
    public ArrayList<info.hccis.courtbooking.model.jpa.CodeValue> list(@WebParam(name = "codeId") int codeId, @WebParam(name = "dbName") String dbName, @WebParam(name = "userName") String userName, @WebParam(name = "pw") String pw) {
        DatabaseConnection databaseConnection = new DatabaseConnection(dbName, userName, pw);
        return CodeValueDAO.getCodeValues(databaseConnection, String.valueOf(codeId));
    }
}
