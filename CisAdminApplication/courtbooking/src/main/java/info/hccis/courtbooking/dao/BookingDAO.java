package info.hccis.courtbooking.dao;

import info.hccis.courtbooking.dao.util.ConnectionUtils;
import info.hccis.courtbooking.dao.util.DbUtils;
import info.hccis.courtbooking.model.DatabaseConnection;
import info.hccis.courtbooking.model.jpa.Booking;
import info.hccis.courtbooking.model.jpa.Court;
import info.hccis.courtbooking.model.jpa.User;
import info.hccis.courtbooking.util.Utility;
import static info.hccis.courtbooking.util.Utility.addDays;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BookingDAO {

    /*
    * creates array of all bookings
    * 
    *@author Apound
    *@since Dec 2017
     */
    public static ArrayList<Booking> getBookingsString(DatabaseConnection databaseConnection) {
        ArrayList<Booking> bookings = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "SELECT * FROM `Booking` order by bookingId";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) { //loops through all bookings and creates a booking object in the array for each
                Booking booking = new Booking();
                booking.setBookingId(rs.getInt("bookingId"));
                booking.setUserId(rs.getInt("userId"));
                booking.setCourtId(rs.getInt("courtId"));
                booking.setOpponent(rs.getInt("opponent"));
                booking.setBookingDate(rs.getString("bookingDate"));
                booking.setCreatedDateTime(rs.getDate("createdDateTime"));
                booking.setCreatedUserId(rs.getString("createdUserId"));
                bookings.add(booking);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return bookings;
    }

    public static ArrayList<Court> getAvailableBookings(String startDate, String endDate, int courtType) {
        System.out.println("Running getAvailableBookings service.");
        ArrayList<Booking> bookings = new ArrayList();
        ArrayList<Court> courts = new ArrayList();
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");  //formats date
        SimpleDateFormat serviceFormat = new SimpleDateFormat("MM-dd-yyyy");  //formats date from service
        ArrayList<Date> requestedDates = new ArrayList();

        String useStartDate = startDate;
        String useEndDate = endDate;
        Date tempDate = null;
        Date startD = null;
        Date endD = null;

        try {
            startD = serviceFormat.parse(startDate);
        } catch (ParseException ex) {
            System.out.println("I failed (startDate)");
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            endD = serviceFormat.parse(endDate);
        } catch (ParseException ex) {
            System.out.println("I failed (endDate)");
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        Date currentDate = new Date();
        currentDate = startD;
        while (currentDate.before(endD)) {
            requestedDates.add(currentDate);
            currentDate = addDays(currentDate, 1);

        }
        if (!currentDate.equals(endD)) {
            requestedDates.add(endD);
        }

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM `Booking` order by bookingId";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) { //loops through all bookings and creates a booking object in the array for each
                Booking booking = new Booking();
                booking.setBookingId(rs.getInt("bookingId"));
                booking.setUserId(rs.getInt("userId"));
                booking.setCourtId(rs.getInt("courtId"));
                booking.setOpponent(rs.getInt("opponent"));
                booking.setBookingDate(rs.getString("bookingDate"));
                booking.setCreatedDateTime(rs.getDate("createdDateTime"));
                booking.setCreatedUserId(rs.getString("createdUserId"));
                bookings.add(booking);

            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        }

        ps = null;
        sql = null;
        conn = null;

        try {
            conn = ConnectionUtils.getConnection();
            sql = "SELECT * FROM `Court` WHERE courtType = ? order by courtId";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, courtType);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) { //loops through all bookings and creates a booking object in the array for each
                Court court = new Court();
                court.setCourtId(rs.getInt("courtId"));
                court.setCourtType(rs.getInt("courtType"));
                courts.add(court);

            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        System.out.println(courts.size());
        for (int i = 0; i < courts.size(); i++) {
            ArrayList<Date> courtAvailableDates = new ArrayList();
            currentDate = startD;
            //rebuild array of requested dates because for some reason it wouldnt work otherwise
            while (currentDate.before(endD)) {
                courtAvailableDates.add(currentDate);
                currentDate = addDays(currentDate, 1);
            }
            courtAvailableDates.add(endD);
            for (int j = 0; j < bookings.size(); j++) {
                if (courts.get(i).getCourtId() == bookings.get(j).getCourtId()) {
                    for (int k = 0; k < courtAvailableDates.size(); k++) {
                        try {
                            tempDate = format.parse(bookings.get(j).getBookingDate());
                        } catch (ParseException ex) {
                            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if (tempDate.equals(courtAvailableDates.get(k))) {
                            tempDate = requestedDates.get(k);
                            courtAvailableDates.remove(tempDate);
                        }
                    }
                }
            }
            //Set transient properties for service
            courts.get(i).setSearchStartDate(useStartDate);
            courts.get(i).setSearchEndDate(useEndDate);
            courts.get(i).setAvailableDates(courtAvailableDates);
            courtAvailableDates = null;
        }
        return courts;
    }

}
