package info.hccis.courtbooking.web;

import info.hccis.booking.bo.BookingBO;
import info.hccis.courtbooking.data.springdatajpa.BookingRepository;
import info.hccis.courtbooking.data.springdatajpa.CodeTypeRepository;
import info.hccis.courtbooking.data.springdatajpa.CodeValueRepository;
import info.hccis.courtbooking.data.springdatajpa.CourtRepository;
import info.hccis.courtbooking.data.springdatajpa.UserRepository;
import info.hccis.courtbooking.model.DatabaseConnection;
import info.hccis.courtbooking.model.jpa.Booking;
import info.hccis.courtbooking.model.jpa.CodeValue;
import info.hccis.courtbooking.model.jpa.Court;
import info.hccis.courtbooking.model.jpa.User;
import info.hccis.courtbooking.util.Utility;
import info.hccis.user.bo.UserBO;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author APound
 * @since Dec 2017
 */
@Controller
public class BookingController {

    private final BookingRepository br;
    private final CourtRepository cr;
    private final CodeTypeRepository ctr;
    private final CodeValueRepository cvr;
    private final UserRepository ur;

    @Autowired
    public BookingController(BookingRepository br, CourtRepository cr, CodeTypeRepository ctr, CodeValueRepository cvr, UserRepository ur) {
        this.br = br;
        this.cr = cr;
        this.ctr = ctr;
        this.cvr = cvr;
        this.ur = ur;
    }
    private MessageSource messageSource;  //used to get filename from message.properties

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @RequestMapping("/bookings/listAvailable")
    public String showAvailableBookings(Model model, HttpSession session, HttpServletRequest request) {
        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
        ArrayList<Booking> bookings = (ArrayList<Booking>) br.findAll();
        model.addAttribute("bookings", bookings);
        return "/bookings/listAvailable";
    }

    @RequestMapping("/bookings/add")
    public String populateBooking(Model model, HttpSession session, HttpServletRequest request) {

        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
        ArrayList<CodeValue> courtTypes = (ArrayList<CodeValue>) cvr.findByCodeTypeId(2);  //returns only court code types
        model.addAttribute("courtTypes", courtTypes);
        System.out.println("Found " + courtTypes.size() + " courts"); //displays to console the number of court types found
        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        ArrayList<User> opponents = (ArrayList<User>) ur.findAll();  //find all potential opponents (ie. users)
        for (int i = 0; i < opponents.size(); i++) {  //loops through opponents and removes logged in user from list
            if (opponents.get(i).getUserId() == user.getUserId()) {
                opponents.remove(i);
            }
        }
        model.addAttribute("opponents", opponents);
        if (user == null) {  //if user isn't logged in, send to add user page
            model.addAttribute("user", new User());
            return "users/add";
        }
        return "bookings/addSubmit";

    }

    @RequestMapping("/bookings/addSubmit")
    public String addBooking(Model model, HttpSession session, HttpServletRequest request, @RequestParam int dropCourtType, @RequestParam String bookingDate) {
        CodeValue codeValue = cvr.findByCodeValueSequence(dropCourtType);  //find what usuer selected from dropdown type
        String courtDescription = codeValue.getEnglishDescription(); //figures out what the english desciption of the dropdown selection is
        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
        session.setAttribute("bookingDate", bookingDate);
        model.addAttribute("bookingDate", bookingDate);
        Court checkCourt = new Court();
        int courtType = dropCourtType;
        ArrayList<Booking> dateBookings = (ArrayList<Booking>) br.findByBookingDate(bookingDate);  //finds all existing bookings on booking date
        ArrayList<Court> selectedCourts = (ArrayList<Court>) cr.findByCourtType(courtType);  //finds all courts for selected court type
        ArrayList<Court> availableCourts = new ArrayList(); //creates an empty array of available courts
        ArrayList<User> opponents = (ArrayList<User>) ur.findAll(); //populates an array of available opponents
        int removeThisCourt = -1;
        boolean available = true;
        if (selectedCourts.size() > 0) { //checks if any courts exist for the selected type
            for (int i = 0; i < selectedCourts.size(); i++) {  //loops through all courts of selected type
                checkCourt = selectedCourts.get(i);
                for (int j = 0; j < dateBookings.size(); j++) { //loops through all bookings on selected date
                    if (checkCourt.getCourtId() == dateBookings.get(j).getCourtId()) { //checks if booking is for the current court in the loop
                        available = false; //if court is booked, sets flag to false
                    }
                }
                if (available) { //if flag is still true then court is available on selected date, add court to array  
                    availableCourts.add(checkCourt);
                }
                available = true;  //reset flag for next court loop
            }
            if (availableCourts.isEmpty()) { //checks i there are NO available courts
                System.out.println("No Available Courts");
                ArrayList<CodeValue> courtTypes = (ArrayList<CodeValue>) cvr.findByCodeTypeId(2);  //refreshs list of available courts, incase changes were made after user made selection
                model.addAttribute("courtTypes", courtTypes);
                model.addAttribute("message", "There are no available bookings on this date for this type of court."); //add message for user display on return to add page
                return "bookings/add";
            } else {
                //repopulates the available courts based on the selection, as well as the selected booking date and list of potential opponents
                model.addAttribute("selectedCourts", availableCourts);
                session.setAttribute("selectedCourts", availableCourts);
                session.setAttribute("bookingDate", bookingDate);
                model.addAttribute("opponents", opponents);
                //make sure logged in
                User user = (User) session.getAttribute("loggedInUser");
                if (user == null) {
                    model.addAttribute("user", new User());  //if user isnt logged in, return to add page
                    return "users/add";
                }
                return "bookings/listAvailable";  //returns to list of available courts for selection
            }
        } else {
            //if there were no available courts, repopulate model attributes and return to add page
            ArrayList<CodeValue> courtTypes = (ArrayList<CodeValue>) cvr.findByCodeTypeId(2);
            model.addAttribute("courtTypes", courtTypes);
            model.addAttribute("message", "There are no available bookings on this date for this type of court.");
            return "bookings/add";
        }

    }

    @RequestMapping("/bookings/commitBooking")
    public String addThisBooking(Model model, HttpSession session, HttpServletRequest request, @RequestParam int dropOpp, @ModelAttribute("selectedCourt") Court selectedCourt) {
        String fileName = messageSource.getMessage("booking.out", null, Locale.US);  //sets filename to text from message.properties as per project requirement
        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
        ArrayList<CodeValue> codeValue = (ArrayList<CodeValue>) cvr.findByCodeTypeId(2);
        model.addAttribute("courtTypes", codeValue);
        User user = (User) session.getAttribute("loggedInUser");  //gets logged in user
        int selectedCourtId = (int) session.getAttribute("selectedCourtId"); //geets selected court id for reservation
        ArrayList<Booking> refreshedBookings = (ArrayList<Booking>) br.findByCourtId(selectedCourtId);        
        String createdUserStr = Integer.toString(user.getUserId()); //converts creating user id to string as per database format
        Timestamp createdDateTime = new Timestamp(new Date().getTime());  //gets current time for entry to database at creation
        Boolean bookingSuccess = false;
        Boolean courtStillAvail = true;
        String bookingDate = "";
        bookingDate = (String) session.getAttribute("bookingDate"); //gets the selected booking date from the session
        Booking addBooking = new Booking(0, user.getUserId(), selectedCourtId, dropOpp, bookingDate, createdUserStr);  //creates a new booking entity from selected attributes
        
        //This section satisfies 2.1.2.2 Validate selection requirement
        addBooking.setCreatedDateTime(createdDateTime);  //sets created timestamp 
        for(int i = 0; i < refreshedBookings.size(); i++){
            if(refreshedBookings.get(i).getBookingDate().equalsIgnoreCase(bookingDate)){
            System.out.println("There was a problem adding the booking to the database.");
            model.addAttribute("message", "I'm sorry, we were not able to reserve this court for you.");
            courtStillAvail = false;
            return "bookings/add";
            }
   
        }
        if(courtStillAvail){
        br.save(addBooking);  //adds booking to database
        }
        ArrayList<Booking> usersBookings = (ArrayList<Booking>) br.findByUserId(user.getUserId());  //gets all user bookings for display and confirmation of booking success
        for (int i = 0; i < usersBookings.size(); i++) {
            if (usersBookings.get(i).getCourtId() == selectedCourtId && usersBookings.get(i).getBookingDate().equalsIgnoreCase(bookingDate)) {  //checks if requested booking was added to database successfully
                addBooking = usersBookings.get(i);
                bookingSuccess = true;
            }
        }
        //displays message to console and to page if reservation was unsuccessfuly
        if (!bookingSuccess) {
            System.out.println("There was a problem adding the booking to the database");
            model.addAttribute("message", "I'm sorry, we were not able to reserve this court for you.");
            return "bookings/add";
        }
        //write to audit file, satisfies requirement 2.5.1 Audit Booking of the project
        try {
            //Create a file
            Files.createDirectories(Paths.get("/cis2232"));
        } catch (IOException ex) {
            System.out.println("io exception caught");
        }
        String textToWrite = "";
        textToWrite = addBooking.getCreatedDateTime() + " " + ur.findByUserId(addBooking.getUserId()).getUsername() + " courtId= " + addBooking.getCourtId() + " " + addBooking.getBookingDate();  //builds string from details for output to audit file
        //Write to the file when a new booking is added
        BufferedWriter bw = null;
        FileWriter fw = null;
        try {
            fw = new FileWriter(fileName, true);
            bw = new BufferedWriter(fw);
            bw.write(textToWrite);
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        User oppUser = new User();
        String oppName = "";
        String courtDesc = "";
        Booking thisBooking = new Booking();
        //loops through bookings and sets transient attributes for opponent name and court description
        for (int j = 0; j < usersBookings.size(); j++) {
            thisBooking = usersBookings.get(j);
            courtDesc = cvr.findByCodeValueSequence(cr.findByCourtId(thisBooking.getCourtId()).getCourtType()).getEnglishDescription();
            try {
                thisBooking.setCourtDescription(courtDesc);
            } catch (Exception e) {
                thisBooking.setCourtDescription("Unknown");
            }
            usersBookings.set(j, thisBooking);
            if (thisBooking.getOpponent() == 0) {
                //if opponent was left blank, populate with "unknown"
                thisBooking.setOpponentName("Unknown");
            } else {
                oppUser = ur.findByUserId(thisBooking.getOpponent());
                oppName = oppUser.getFirstName() + " " + oppUser.getLastName();
                try {
                    thisBooking.setOpponentName(oppName);
                } catch (Exception e) {
                    thisBooking.setOpponentName("Unknown");
                }
            }
        }
        model.addAttribute("usersBookings", usersBookings);  //adds users bookings to model for display
        return "bookings/userBookings";
    }

    @RequestMapping("/bookings/addSubmitStepTwo")
    public String BackupAddBooking(Model model, HttpSession session, HttpServletRequest request) {

        String courtId = request.getParameter("courtId");
        int courtIdInt = Integer.parseInt(courtId);
        Court court = cr.findByCourtId(courtIdInt);
        Booking booking = new Booking();
        booking.setCourtId(courtIdInt);
        session.setAttribute("selectedCourtId", courtIdInt);
        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
        String bookingDate = "";
        bookingDate = (String) session.getAttribute("bookingDate");
        ArrayList<User> opponents = (ArrayList<User>) ur.findAll();
        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "users/add";
        }
        for (int i = 0; i < opponents.size(); i++) {
            if (opponents.get(i).getUserId() == user.getUserId()) {
                opponents.remove(i);
            }
        }
        model.addAttribute("opponents", opponents);
        model.addAttribute("selectedCourt", court);
        return "bookings/addSubmit";
    }
    
    
    
    

    @RequestMapping("/bookings/reloadAddScreen")
    public String reloadAddScreen(Model model, HttpSession session) {
        User user = (User) session.getAttribute("loggedInUser");
        if(user == null){
            model.addAttribute("user", new User());
            session.setAttribute("loggedInUser", user);
            model.addAttribute("message", "Please log in to view bookings");
            return "other/welcome";
        } else{
        System.out.println(user.toString());
        }  
        ArrayList<CodeValue> codeValue = (ArrayList<CodeValue>) cvr.findByCodeTypeId(2);
        model.addAttribute("courtTypes", codeValue);
        int numOfBookings = 0;
        numOfBookings = BookingBO.authenticate(user, br);  //checks if user has too many bookings
        System.out.println("User has "+numOfBookings+" in the future");
        if (numOfBookings > 1) {
            System.out.println("You have too many bookings, you have (" + numOfBookings + ")");
            model.addAttribute("message", "You have too many bookings, you can only have 2 future bookings");
        }
        System.out.println();
        System.out.println();
        System.out.println("User is "+ user.getFirstName());
        ArrayList<Booking> usersBookings = (ArrayList<Booking>) br.findByUserId(user.getUserId());
        Booking thisBooking = new Booking();
        User oppUser = new User();
        String courtDesc = "";
        String oppName = "";
        //populates transient properties of user bookings for display
        for (int i = 0; i < usersBookings.size(); i++) {
            thisBooking = usersBookings.get(i);
            courtDesc = cvr.findByCodeValueSequence(cr.findByCourtId(thisBooking.getCourtId()).getCourtType()).getEnglishDescription();
            try {
                thisBooking.setCourtDescription(courtDesc);
            } catch (Exception e) {
                thisBooking.setCourtDescription("Unknown");
            }
            usersBookings.set(i, thisBooking);
            if (thisBooking.getOpponent() == 0) {
                thisBooking.setOpponentName("Unknown");
            } else {
                oppUser = ur.findByUserId(thisBooking.getOpponent());
                oppName = oppUser.getFirstName() + " " + oppUser.getLastName();
                try {
                    thisBooking.setOpponentName(oppName);
                } catch (Exception e) {
                    thisBooking.setOpponentName("Unknown");

                }
            }
        }
        model.addAttribute("usersBookings", usersBookings);
        return "bookings/userBookings";
    }

    @RequestMapping("/bookings/reloadAdd")
    public String reloadAdd(Model model, HttpSession session) {
        User user = (User) session.getAttribute("loggedInUser");
        ArrayList<CodeValue> codeValue = (ArrayList<CodeValue>) cvr.findByCodeTypeId(2);
        model.addAttribute("courtTypes", codeValue);
        int numOfBookings = 0;
        numOfBookings = BookingBO.authenticate(user, br);
        if (numOfBookings > 1) {
            String courtDesc = "";
            User oppUser = new User();
            String oppName = "";
            System.out.println("You have too many bookings, you have (" + numOfBookings + ")");
            model.addAttribute("message", "You have too many bookings, you can only have 2 future bookings");
            Booking thisBooking = new Booking();
            ArrayList<Booking> usersBookings = (ArrayList<Booking>) br.findByUserId(user.getUserId());
            for (int i = 0; i < usersBookings.size(); i++) {
                thisBooking = usersBookings.get(i);
                courtDesc = cvr.findByCodeValueSequence(cr.findByCourtId(thisBooking.getCourtId()).getCourtType()).getEnglishDescription();
                try {
                    thisBooking.setCourtDescription(courtDesc);
                } catch (Exception e) {
                    thisBooking.setCourtDescription("Unknown");
                }
                usersBookings.set(i, thisBooking);
                if (thisBooking.getOpponent() == 0) {
                    thisBooking.setOpponentName("Unknown");
                } else {
                    oppUser = ur.findByUserId(thisBooking.getOpponent());
                    oppName = oppUser.getFirstName() + " " + oppUser.getLastName();
                    try {
                        thisBooking.setOpponentName(oppName);
                    } catch (Exception e) {
                        thisBooking.setOpponentName("Unknown");
                    }
                }
            }
            model.addAttribute("usersBookings", usersBookings);
            return "bookings/userBookings";
        }
        ArrayList<Booking> usersBookings = (ArrayList<Booking>) br.findByUserId(user.getUserId());
        Booking thisBooking = new Booking();
        User oppUser = new User();
        String courtDesc = "";
        String oppName = "";
        for (int i = 0; i < usersBookings.size(); i++) {
            thisBooking = usersBookings.get(i);
            courtDesc = cvr.findByCodeValueSequence(cr.findByCourtId(thisBooking.getCourtId()).getCourtType()).getEnglishDescription();
            try {
                thisBooking.setCourtDescription(courtDesc);
            } catch (Exception e) {
                thisBooking.setCourtDescription("Unknown");
            }
            usersBookings.set(i, thisBooking);
            if (thisBooking.getOpponent() == 0) {
                thisBooking.setOpponentName("Unknown");
            } else {
                oppUser = ur.findByUserId(thisBooking.getOpponent());
                oppName = oppUser.getFirstName() + " " + oppUser.getLastName();
                try {
                    thisBooking.setOpponentName(oppName);
                } catch (Exception e) {
                    thisBooking.setOpponentName("Unknown");

                }
            }
        }
        model.addAttribute("usersBookings", usersBookings);
        return "bookings/add";
    }

}
