package info.hccis.courtbooking.web;

import info.hccis.booking.bo.BookingBO;
import info.hccis.courtbooking.data.springdatajpa.BookingRepository;
import info.hccis.courtbooking.data.springdatajpa.CodeTypeRepository;
import info.hccis.courtbooking.data.springdatajpa.CodeValueRepository;
import info.hccis.courtbooking.data.springdatajpa.CourtRepository;
import info.hccis.courtbooking.data.springdatajpa.UserRepository;
import info.hccis.courtbooking.model.DatabaseConnection;
import info.hccis.courtbooking.model.jpa.Booking;
import info.hccis.courtbooking.model.jpa.CodeType;
import info.hccis.courtbooking.model.jpa.CodeValue;
import info.hccis.courtbooking.model.jpa.User;
import info.hccis.courtbooking.util.Utility;
import info.hccis.user.bo.UserBO;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    private final CodeTypeRepository ctr;
    private final CodeValueRepository cvr;
    private final BookingRepository br;
    private final UserRepository ur;
    private final CourtRepository cr;

    @Autowired
    public OtherController(CodeTypeRepository ctr, CodeValueRepository cvr, BookingRepository br, UserRepository ur, CourtRepository cr) {
        this.ctr = ctr;
        this.cvr = cvr;
        this.br = br;
        this.ur = ur;
        this.cr = cr;
    }

    @RequestMapping("/other/futureUse")
    public String showFutureUse(Model model) {
        return "other/futureUse";
    }

    @RequestMapping("/other/about")
    public String showAbout(Model model) {
        return "other/about";
    }

    @RequestMapping("/other/help")
    public String showHelp(Model model) {
        return "other/help";
    }

    @RequestMapping("/")
    public String showHome(Model model, HttpSession session) {

        model.addAttribute("user", new User());
        ArrayList<CodeValue> codeValue = new ArrayList<>();
        model.addAttribute("codeValue", codeValue);
        ArrayList<CodeValue> courtTypes = (ArrayList<CodeValue>) cvr.findByCodeTypeId(2);  //should probably replace 2 with a constant variable
        model.addAttribute("courtTypes", courtTypes);
        session.removeAttribute("loggedInUser");
        //This will send the user to the welcome.html page.
        //setup the databaseConnection object in the model.  This will be used on the 
        //view.
        System.out.println("MD5 hash for 123=" + Utility.getHashPassword("123"));
        DatabaseConnection databaseConnection = new DatabaseConnection();
        model.addAttribute("databaseConnection", databaseConnection);
        session.setAttribute("db", databaseConnection);
        return "other/welcome";
    }

    @RequestMapping("/logout")
    public String logout(Model model, HttpSession session) {

        model.addAttribute("user", new User());
        session.removeAttribute("loggedInUser");
        //Give a message indicating that they have been logged out.
        model.addAttribute("message", "Successfully logged out");
        //This will send the user to the welcome.html page.
        return "other/welcome";
    }

    @RequestMapping("/authenticate")
    public String authenticate(Model model, @ModelAttribute("user") User user, HttpSession session) {

        if (!UserBO.authenticate(user, ur)) {
            //failed validation
            session.setAttribute("loggedInUser", user);
            model.addAttribute("message", "Authentication failed");
            return "other/welcome";
        } else {
            User thisUser = UserBO.getUserByUsername(user, ur);
            System.out.println();
            session.setAttribute("loggedInUser", thisUser);

            System.out.println();

            ArrayList<CodeValue> codeValue = (ArrayList<CodeValue>) cvr.findByCodeTypeId(2);
            model.addAttribute("courtTypes", codeValue);
            System.out.println("ALP-found " + codeValue.size() + " courtTypes.  Going to welcome page");
            System.out.println();
            int numOfBookings = 0;
            numOfBookings = BookingBO.authenticate(thisUser, br);  //validates number of bookings
           
                ArrayList<Booking> usersBookings = (ArrayList<Booking>) br.findByUserId(thisUser.getUserId());           
                
                Booking thisBooking = new Booking();
                String courtDesc = "";
                User oppUser = new User();
                String oppName = "";
                for (int i = 0; i < usersBookings.size(); i++) {
                    thisBooking = usersBookings.get(i);
                    courtDesc = cvr.findByCodeValueSequence(cr.findByCourtId(thisBooking.getCourtId()).getCourtType()).getEnglishDescription();
                    try {
                        thisBooking.setCourtDescription(courtDesc);
                    } catch (Exception e) {
                        thisBooking.setCourtDescription("Unknown");
                    }
                    usersBookings.set(i, thisBooking);
                    if (thisBooking.getOpponent() == 0) {
                        thisBooking.setOpponentName("Unknown");
                    } else {
                        oppUser = ur.findByUserId(thisBooking.getOpponent());
                        oppName = oppUser.getFirstName() + " " + oppUser.getLastName();
                        try {
                            thisBooking.setOpponentName(oppName);
                        } catch (Exception e) {
                            thisBooking.setOpponentName("Unknown");
                        }
                    }
                }
            model.addAttribute("usersBookings", usersBookings);
                 if (numOfBookings > 1) {
                // returns list of user bookings if they already have too many bookings
           System.out.println("You have too many bookings, you have (" + numOfBookings + ")");
            model.addAttribute("message", "You have too many bookings, you can only have 2 future bookings");
                return "bookings/userBookings";
            }
           return "bookings/userBookings";

        }
    }
}
