package info.hccis.booking.bo;

import info.hccis.booking.bo.*;
import info.hccis.courtbooking.data.springdatajpa.BookingRepository;
import info.hccis.courtbooking.model.jpa.Booking;
import info.hccis.courtbooking.model.jpa.User;
import info.hccis.courtbooking.util.Utility;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class will contain functionality related to working with user objects.
 *
 * @author apound
 * @since Dec, 2017
 */
public class BookingBO {

    public static boolean validateBooking(Booking booking, BookingRepository br) {
        //Check if the court is booked
     ArrayList<Booking> existingBookings = (ArrayList<Booking>) br.findByCourtId(booking.getCourtId());
        boolean alreadyBooked = false;
        
        
    for(Booking current: existingBookings){
        if (current.getBookingDate().equals(booking.getBookingDate()) ) {
            alreadyBooked = true;
        }
        }

        return alreadyBooked;

    }
    
    public static int authenticate(User user , BookingRepository br) {
       //collects bookings for user
        ArrayList<Booking> bookingsForUser = (ArrayList<Booking>) br.findByUserId(user.getUserId());
         int daysBooked = 0; //days booked by user
        if (bookingsForUser.size() > 0){ //checks if user has any bookings
        System.out.println("found "+ bookingsForUser.size() + " bookings for user"); //displays to console number of results found
        Date reqdDate = new Date(System.currentTimeMillis());  //variable for current date
        Date bookedOn = new Date(); //date object to hold the bookings date for comparison
        SimpleDateFormat format = new SimpleDateFormat("mm/dd/yyyy");  //formats date
        for (int i= 0 ; i < bookingsForUser.size(); i++){ //loops through users bookings  
            try {
                bookedOn = format.parse(bookingsForUser.get(i).getBookingDate()); //converts string to date format for comparison
                        
            } catch (ParseException ex) {
                Logger.getLogger(info.hccis.booking.bo.BookingBO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if(reqdDate.before(bookedOn)){ //checks if user's booking is in the future
                daysBooked = daysBooked + 1; 
 
                
            }
        }
        }
      
        System.out.println("future days booked is " + daysBooked); //displays to console the number of days the user has booked in the future
        return daysBooked;
        

    }
}
    
    
