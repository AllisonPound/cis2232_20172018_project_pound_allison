package fileiotesta;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is a program to work with files.
 *
 * @since 2017-09-13
 * @author bjmaclean
 */
public class FileIOTestA {

    private static final String FILENAME = "\\cis2232\\TEST.txt";

    public static void main(String[] args) {

        Path path = Paths.get(FILENAME);
        System.out.println(path);
        System.out.println(path.toAbsolutePath());

        try {
            //http://docs.oracle.com/javase/7/docs/api/java/nio/file/attribute/BasicFileAttributes.html
            BasicFileAttributes attrs = Files.readAttributes(path, BasicFileAttributes.class);
            System.out.println(attrs.creationTime());
        } catch (IOException ex) {
            System.out.println(ex);
        }

        //Write code to read the file and output the contents to the console.
        BufferedReader br = null;
        FileReader fr = null;

        try {

            //br = new BufferedReader(new FileReader(FILENAME));
            fr = new FileReader(path.toString());
            br = new BufferedReader(fr);

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
            }

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (br != null) {
                    br.close();
                }

                if (fr != null) {
                    fr.close();
                }

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }

    }
}
