package info.hccis.cis2232example;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bjmaclean
 */
public class MainClass {

    public static String MENU = "Options:\nE) Edit camper\nS) Show campers\nX) Exit";
    public static String FILE_NAME = "/test/campersRandom.txt";

    public static void main(String[] args) throws IOException {

        //********************************************************
        // BJM
        // Write an object to a file.
        //********************************************************
        
        FileUtility.writeObject(new Camper(), FILE_NAME+"OBJECT");
        
        //********************************************************
        //Create a file
        //********************************************************
        FileUtility.createFileInitialized(FILE_NAME);
        
        String option = "";
        ArrayList<Camper> theList = new ArrayList();
        loadCampersFromRandom(theList);
        do {
            System.out.println(MENU);
            option = FileUtility.getInput().nextLine().toUpperCase();

            switch (option) {
                case "E":
                    //System.out.println("Picked A");
                    System.out.println("Enter id");
                    int id = Integer.parseInt(FileUtility.getInput().nextLine());
                    
                    Camper theCamper = null;
                    for(Camper camper: theList){
                        if(camper.getRegistrationId() == id){
                            theCamper = camper;
                            break;
                        }
                    }
                    theCamper.edit(FILE_NAME);
                    FileUtility.writeCamperRandom(theCamper, FILE_NAME);
//                    FileUtility.writeLine(FILE_NAME, newCamper.getJson());
                    break;
                case "S":
                    System.out.println("Here are the campers");
                    for (Camper camper : theList) {
                        System.out.println(camper);
                    }
                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");
                    break;

            }
        } while (!option.equalsIgnoreCase("x"));
    }

    /**
     * This method will load the campers from the file at the program startup.
     *
     * @param campers
     * @since 20150917
     * @author BJ MacLean
     */
    public static void loadCampers(ArrayList campers) {
        System.out.println("Loading campers from file");
        BufferedReader theReader = FileUtility.getBufferedReader(FILE_NAME);
        
        String line = FileUtility.readLine(theReader);
        int count = 0;
        while (line != null) {
            count++;
            Gson gson = new Gson();
            campers.add(gson.fromJson(line, Camper.class));
            //check max reg id...have to set this to be sure.
            System.out.println(line);
            line = FileUtility.readLine(theReader);
        }
        try {
            theReader.close();
        } catch (IOException ex) {
            System.out.println("error closing.");
        }
        System.out.println("Finished...Loading campers from file (Loaded " + count + " campers)");

    }

    public static void loadCampersFromRandom(ArrayList campers) {

        System.out.println("Loading campers from file");
        
        BufferedReader theReader = FileUtility.getBufferedReader(FILE_NAME);
        for(int i = 1; i<=Camper.NUMBER_CAMPERS; ++i){
            campers.add(loadACamperFromRandom(i, theReader));
        }

        try {
            theReader.close();
        } catch (IOException ex) {
            Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Finished...Loading campers from file (Loaded " + campers.size() + " campers)");

    }


    /**
     * This method will load a camper from a random access file.
     *
     * @since 20150917
     * @author BJ MacLean
     */
    public static Camper loadACamperFromRandom(int recordNumber, BufferedReader theReader ) {
        
        char[] chars = new char[Camper.RECORD_SIZE];
        try {
            System.out.println("loading:  "+recordNumber);
            theReader.read(chars, 0, Camper.RECORD_SIZE);
        } catch (IOException ex) {
            Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        Gson gson = new Gson();
        String line = new String(chars);
        return gson.fromJson(line, Camper.class);
    }
        

}
