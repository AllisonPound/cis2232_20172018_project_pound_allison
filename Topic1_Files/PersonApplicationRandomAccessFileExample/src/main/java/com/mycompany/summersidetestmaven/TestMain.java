package com.mycompany.summersidetestmaven;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;

public class TestMain {
    public static final int RECORD_LENGTH = 100;
    public static final String FILE_NAME = "/cis2232/test.txt";

    public static void main(String[] args) {
        System.out.println("Test from a maven app.");

        
        String test = "                                                                                                    ";
        System.out.println(test.length());
        Person person = new Person();
        person.setLastName("Smith");
        person.setFirstName("Ryan");
        person.setId(2);
        
        Gson gson = new Gson();
        String jsonStringForPerson = gson.toJson(person);
        System.out.println(jsonStringForPerson);

        try {
            File file = new File(FILE_NAME);

            if (file.exists()) {
                System.out.println("File existed");
            } else {
                System.out.println("File not found!");
                FileWriter fw = new FileWriter(file);
                for(int i = 0; i<100; i++){
                    fw.write("          ");
                }
                fw.close();
                System.out.println("Wrote to the file");
            }
            RandomAccessFile raf = new RandomAccessFile(file, "rw");
          
            int locationToStart = (person.getId()*RECORD_LENGTH-100)    ;
            raf.seek(locationToStart);
            raf.writeBytes(jsonStringForPerson);
            raf.close(); 
             
            

        } catch (Exception ioe) {
            System.out.println(ioe);
        }

    }

}
