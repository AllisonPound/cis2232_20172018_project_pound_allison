package info.hccis.cis2232example;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author bjmaclean
 */
public class MainClass {

    public static String MENU = "Options:\nA) Add camper\nS) Show campers\nX) Exit";
    public static String FILE_NAME = "/test/campers.txt";

    public static void main(String[] args) throws IOException {
        //Create a file
        ArrayList<Camper> theList = new ArrayList();
        loadCampers(theList);
        String option;
        do {
            System.out.println(MENU);
            option = FileUtility.getInput().nextLine().toUpperCase();

            switch (option) {
                case "A":
                    //System.out.println("Picked A");
                    Camper newCamper = new Camper(true);
                    theList.add(newCamper);
                    break;
                case "S":
                    System.out.println("Here are the campers");
                    for(Camper camper: theList){
                        System.out.println(camper);
                    }
                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");
                    break;

            }
        } while (!option.equalsIgnoreCase("x"));
    }

    /**
     * This method will load the campers from the file at the program startup.
     *
     * @param campers
     * @since 20150917
     * @author BJ MacLean
     */
    public static void loadCampers(ArrayList campers) {
        System.out.println("Loading campers from file");
        int count=0;

        


        System.out.println("Finished...Loading campers from file (Loaded "+count+" campers)");

    }
}
