package com.mycompany.summersidetestjdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This will connect and query from a database.
 *
 * @author bjmaclean
 * @since Sep 22, 2017
 */
public class Main {

    public static void main(String[] args) {
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/cis2232_cisadmin2",
                "cis2232_admin",
                "Test1234")) {
            /* you use the connection here */

            try (Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery("SELECT * FROM User")) {
                while (rs.next()) {
                    int numColumns = rs.getMetaData().getColumnCount();
                    for (int i = 1; i <= numColumns; i++) {
                        // Column numbers start at 1.
                        // Also there are many methods on the result set to return
                        //  the column as a particular type. Refer to the Sun documentation
                        //  for the list of valid conversions.
                        System.out.println("COLUMN " + i + " = " + rs.getObject(i));
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }  // the VM will take care of closing the connection

    }

}
