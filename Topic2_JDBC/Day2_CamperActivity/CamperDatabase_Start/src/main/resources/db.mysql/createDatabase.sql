create database canes;
use canes;

/*create a user in database*/
grant select, insert, update, delete on canes.*
to 'cis2232_admin'@'localhost'
identified by 'Test1234';
flush privileges;

CREATE TABLE IF NOT EXISTS `Camper` (
`id` int(4) unsigned NOT NULL COMMENT 'Registration id',
`firstName` varchar(10) DEFAULT NULL,
`lastName` varchar(10) DEFAULT NULL,
`dob` varchar(10) DEFAULT NULL
);