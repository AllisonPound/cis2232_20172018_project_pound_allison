﻿### What is this repository for? ###

This repository will be used to provide the end term project as per the requirements outlined in the Project Design Document below
This project is being completed individually due to my partner being unreachable and I have been assigned "Team Member 1"'s responsibilities

### Who do I talk to? ###

* Allison Pound
* CIS Holland College

###Project Outline ###
––CIS2232 Advanced OOP

Term Project
Court Booking System
Fall 2017 


The database creation script for this application is located in the project under Other Sources > src/main/resources > db.mysql > "createDatabase.sql"

The URL for the webservice is:

http://localhost:8080/courtbooking/rest/CourtBookingService/availableCourts/**StartDate(MM-dd-yyyy format)**/**EndDate(MM-dd-yyyy format)**/**Court Type (int)**

No special steps are required to launch the project, simply:
- open in netbeans
- run database creation script
- run courtbooking project

Any questions or concerns can be directed to Allison Pound at apound112143@hollandcollege.com
